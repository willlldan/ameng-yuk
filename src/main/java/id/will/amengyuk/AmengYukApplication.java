package id.will.amengyuk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmengYukApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmengYukApplication.class, args);
	}

}
